using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HSDisplay : MonoBehaviour
{
    public SavedData data;
    // Start is called before the first frame update
    void Start()
    {
        data.LoadFile();
        GetComponent<TMPro.TMP_Text>().text = "Highscore:" + data.bestScore;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
