using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Consumable", menuName = "Scriptables/Consumable", order = 1)]

public class Consumable : ItemBase
{
   public enum Type{
    multiplier,
    restorer,
    ammo
   }
   public Type consumableType;
   public float powerX;
   public float powerY;
   public float powerZ;
   public float duration;

   public void Consume()
   {
        switch (consumableType){
            case Type.multiplier:
                Multiply();
                break;
            case Type.restorer:
                restore();
                break;
            case Type.ammo:
                reload();
            break;
        }
   }
   public void Multiply(){
   PowerUpManager PuM = GameObject.Find("Player").GetComponent<PowerUpManager>();
   if (powerX != 1)
   PuM.DamageMultiplyBegin(powerX, duration);
   if (powerY != 1)
   PuM.DefenseMultiplyBegin(powerY, duration);
   if (powerZ != 1)
   PuM.SpeedMultiplyBegin(powerZ, duration);
   }
public void restore(){
PlayerController pc = GameObject.Find("Player").GetComponent<PlayerController>();
pc.data.health += powerX;
}
public void reload(){
PlayerController pc = GameObject.Find("Player").GetComponent<PlayerController>();
pc.weaponC.data.ammo += pc.weaponC.data.loadValue;
}

}