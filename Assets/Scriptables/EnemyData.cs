using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "Scriptables/Enemy", order = 1)]
public class EnemyData : ScriptableObject
{
    public GameObject enemyPrefab;
    public float baseHealth;
    public float SpawnCD;
    public List<GameObject> drops;
    public int scoreValue;
    public int coinValue;

}
