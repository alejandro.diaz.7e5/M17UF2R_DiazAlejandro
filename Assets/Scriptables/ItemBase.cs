using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBase : ScriptableObject
{
    public enum itemType
    {
        weapon,
        item
    }
   public itemType type;
   public Sprite Image;
   public string Description;
   public float BasePrice;
   public int minAmount;
   public int maxAmount;
   public int currentAmount;

   public void Use(){
      Debug.Log("using item");
      if (this is Consumable)
      {
         Debug.Log("consuming item...");
         ((Consumable)this).Consume();
         currentAmount--;
         if (currentAmount <= 0)
           GameObject.Find("Player").GetComponent<PlayerController>().data.items.Remove(this);
         
      }
   }
}
