using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemList", menuName = "Scriptables/ItemList", order = 1)]

public class ItemLists : ScriptableObject
{
    public List<WeaponData> weapons;
    public List<GameObject> powerUp;
}
