using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Scriptables/Music", fileName = "Music List")]
public class MusicList : ScriptableObject
{
    public List<AudioClip> Music;  
}
