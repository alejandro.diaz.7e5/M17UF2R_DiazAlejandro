using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Scriptables/Player", order = 1)]
public class PlayerData : ScriptableObject
{
    public float health;
    public WeaponData[] weapons = new WeaponData[5];
    public List<ItemBase> items;
    public int score;
    public int coins;
    public int defeatedEnemies;
    public int completedRooms;
    
}
