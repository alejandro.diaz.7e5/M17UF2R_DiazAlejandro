using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Room Spawn", menuName = "Scriptables/Room Spawn", order = 1)]
public class RoomSpawnPoints : ScriptableObject
{
    public List<Vector3> spawnPoints;
    public Vector3 shopSpawnPoint;
    public int[] proceduralSpawnSequence;
    public int CurrentRoom = 0;
    public void Next()
    {
        GameObject.FindObjectOfType<SpawnerManager>().ActivateSpawn(proceduralSpawnSequence[CurrentRoom]);
        CurrentRoom++;

    }
    public void GenerateSequence()
    {
        CurrentRoom = 0;
        proceduralSpawnSequence = new int[20];
        for (int i = 0; i < proceduralSpawnSequence.Length; i++)
            proceduralSpawnSequence[i] = Random.Range(0, spawnPoints.Count);
    }
}


