using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
[CreateAssetMenu(fileName = "newSave", menuName = "Scriptables/Save")]
public class SavedData : ScriptableObject
{
    public int[] lastScores = { 0, 0, 0 };
    public int bestScore = 0;
    string filePath;

    public void SaveFile()
    {
        Debug.Log(filePath);
        if (!File.Exists(filePath))
            File.Create(filePath).Close();
        using (StreamWriter sw = new StreamWriter(filePath))
        {
            foreach (int score in lastScores)
                sw.Write(score + " ");
            sw.WriteLine();
            sw.WriteLine(bestScore);
        }


    }
    public void LoadFile()
    {
        filePath = Directory.GetCurrentDirectory() + "\\Assets\\Saves\\save";
        if (!File.Exists(filePath))
            SaveFile();
        using (StreamReader sr = new StreamReader(filePath))
        {

            string[] lastScoresData = sr.ReadLine().Split();
            for (int i = 0; i < lastScores.Length; i++)
                lastScores[i] = Convert.ToInt32(lastScoresData[i]);
            bestScore = Convert.ToInt32(sr.ReadLine());
        }
    }
    public void updateLast(int latestScore)
    {
        for (int i = lastScores.Length - 1; i > 0; i--)
            lastScores[i + 1] = lastScores[i];
        lastScores[0] = latestScore;
        SaveFile();

    }

}