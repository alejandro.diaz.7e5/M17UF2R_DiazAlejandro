using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "SceneLoader", menuName = "Scriptables/Scene", order = 1)]
public class SceneLoader : ScriptableObject
{
    public void LoadScene(string Scene)
    {
        SceneManager.LoadScene(Scene);
    }

}
