using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
[CreateAssetMenu(fileName = "SettingsFile",menuName = "Scriptables/Settings")]
public class Settings : ScriptableObject {
 
     public AudioMixer masterMixer;
 
 
     public void SetSfxLevel(float sfxLvl) {
     
         masterMixer.SetFloat ("SfxVol", sfxLvl);
     
     }
 
     public void SetMusicLevel(float musicLvl) {
 
         masterMixer.SetFloat ("MusicVol", musicLvl);
 
     }
 
 
 }