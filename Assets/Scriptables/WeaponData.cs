using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponData", menuName = "Scriptables/Weapon", order = 1)]
public class WeaponData : ItemBase
{
    public Sprite sprite;
    public Color color;
    public int ammo;
    public GameObject bullet;
    public float damage;
    public float shotCoolDown;
    public int shotCount;
    public int loadValue;
    public enum ShotType
    {
        Single,
        Dispersion    
    }
    public ShotType WeaponShot;


}