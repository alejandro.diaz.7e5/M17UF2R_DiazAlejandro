using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletControllerStraight : MonoBehaviour, Damager
{
    // Start is called before the first frame update
    public float speed;
    protected float damage;
    float Damager.damage { get => damage; set => damage = value; }

    void Start()
    {
        transform.Translate(Vector3.right * 2);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
    }
    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
            collision.gameObject.GetComponent<EnemyController>().Damaged(damage);
        if (collision.gameObject.tag != "Bullet")
            Destroy(this.gameObject);

    }
}

