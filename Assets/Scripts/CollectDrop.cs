using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectDrop : MonoBehaviour
{
    public PlayerData data;
    public ItemLists items;
    public int dropId;
    public int extra;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            switch (dropId)
            {
                case 0:
                    for(int i = 0; i < data.weapons.Length; i++)
                    {
                        if (data.weapons[i] == null)
                        {
                            data.weapons[i] = items.weapons[extra];
                            break;
                        }   
                        else if (data.weapons[i] == items.weapons[extra])
                        {
                            data.weapons[i].ammo += data.weapons[i].loadValue;
                            break;
                        }


                    }
                    break;
                case 1:
                    switch (extra)
                    {
                        case 0:
                            data.health += 5;
                            if (data.health > 10) data.health = 10;
                            break;
                        case 1:
                            foreach (var weapon in data.weapons)
                            {
                                if (weapon == null) break;
                                weapon.ammo += 20;
                            }
                            break;
                    }
                    break;
                case 2:
                    collision.gameObject.GetComponent<PowerUpManager>().DamageMultiplyBegin(2, 5);
                    break;
                case 3:
                    collision.gameObject.GetComponent<PowerUpManager>().DefenseMultiplyBegin(2, 5);
                    break;
                case 4:
                    collision.gameObject.GetComponent<PowerUpManager>().SpeedMultiplyBegin(2, 5);
                    break;

            }
            collision.gameObject.GetComponent<PlayerController>().data.score += 3;
            Destroy(this.gameObject);
            
            
        }
    }
}
