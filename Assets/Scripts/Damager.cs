using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Damager
{
    float damage
    {
        get;
        set;
    }
}
