using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2Controller : EnemyController
{

    public GameObject bullet;
    float fireTimer = 0;
    Vector3 direction;

    // Update is called once per frame
    protected override void Attack()
    {
        fireTimer += Time.deltaTime;
        if (fireTimer >= 1)
        {
            fireTimer = 0;
            direction = player.transform.position - transform.position;
            Instantiate(bullet, transform.position + Vector3.down, Quaternion.Euler(0, 0, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg));
        }
    }
}
       