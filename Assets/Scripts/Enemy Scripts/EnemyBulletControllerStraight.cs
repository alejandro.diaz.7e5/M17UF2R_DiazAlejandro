using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletControllerStraight : MonoBehaviour
{
    // Start is called before the first frame update
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right * 3f * Time.deltaTime);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
            collision.gameObject.GetComponent<PlayerController>().Damaged(3);
        if (collision.gameObject.tag != "Enemy")
            Destroy(this.gameObject);
    }
}

