
using System.Collections;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public EnemySpawner spawner;
    public GameObject player;
    public GameObject enemyHealthBar;
    public EnemyData data;
    public bool isStopped = false;
    public AudioClip death;
    public bool deathTriggered;
    protected float life;
    // Start is called before the first frame update
    protected void Start()
    {
        Debug.Log("starting enemy");
        player = GameObject.Find("Player");        
        SetLife();
    }

    // Update is called once per frame
    protected void Update()
    {
        if (!isStopped)
        {
            if (player.transform.position.x < transform.position.x)
                GetComponent<SpriteRenderer>().flipX = true;
            Move();
            Attack();
        }
    }
    public void Damaged(float damage)
    {
        Debug.Log("Hit");
        life -= damage;
        player.GetComponent<PlayerController>().data.score += 1;
        enemyHealthBar.transform.localScale = new Vector3(life, 1, 1);
        if (life <= 0 && !deathTriggered)
        {
            StartCoroutine(Die());
            deathTriggered = true;
       
        }
    }
    public void Stunned()
    {
        Debug.Log("Stunned");
        player.GetComponent<PlayerController>().data.score += 1;
        StartCoroutine(Stun());

    }
    private IEnumerator Stun()
    {
        isStopped = true;
        GetComponent<Animator>().enabled = false;
        yield return new WaitForSeconds(3);
        isStopped = false;
        GetComponent<Animator>().enabled = true;
    }
        
    private IEnumerator Die()
    {
        spawner.EnemyFall();
        isStopped = true;
        GetComponent<Animator>().enabled = false;
        GetComponent<Collider2D>().enabled = false;
        Drop();
        player.GetComponent<PlayerController>().data.score += data.scoreValue;
        player.GetComponent<PlayerController>().data.coins += data.coinValue;
        player.GetComponent<PlayerController>().data.defeatedEnemies++;
        GetComponent<AudioSource>().PlayOneShot(death);
        yield return new WaitForSeconds(1);
        Destroy(this.gameObject);
    }
    void Drop() {
        int rng = Random.Range(0, data.drops.Count);
        if (data.drops[rng] != null)
        {
            Instantiate(data.drops[rng], transform.position, Quaternion.identity);
        }
    }
    protected virtual void Move() { }
    protected virtual void Attack() { }
    void SetLife() {
        life = data.baseHealth;
    }



}
