using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public delegate void AllEnemiesKilled();
    public delegate void Started();
    public event Started OnStart;
    public event AllEnemiesKilled OnAllEnemiesKilled;
    public List<EnemyData> enemies;
    public int[] spawnAmount;
    public int totalEnemies;
    public int defeatedEnemies;
    public Vector2 spawnArea;
    public List<GameObject> exits;
    // Start is called before the first frame update
    void Start()
    {
        OnStart();
        int spawnAmountIndex = 0;

        totalEnemies = spawnAmount.Sum();

        foreach (EnemyData enemy in enemies) 

        {

            StartCoroutine(SpawnEnemies(enemy, spawnAmount[spawnAmountIndex]));
            spawnAmountIndex++; 
        }
    }

    // Update is called once per frame
    IEnumerator SpawnEnemies(EnemyData enemy, int amountToSpawn) 
    {
       
        for (int i = 0; i < amountToSpawn; i++)

        {

            bool validspawn = false;

            Vector3 spawnTarget = Vector3.zero;

            while(validspawn == false)

            {



                spawnTarget = new Vector3(

                this.transform.position.x + UnityEngine.Random.Range(0, spawnArea.x),
                this.transform.position.y + UnityEngine.Random.Range(0, spawnArea.y));

                if (!Physics2D.OverlapCircle(spawnTarget, 1f)) 

                    validspawn = true;           

            }


            GameObject.Instantiate(enemy.enemyPrefab, spawnTarget, Quaternion.identity).GetComponent<EnemyController>().spawner = this;

            yield return new WaitForSeconds(enemy.SpawnCD);



        }
    }
    public void EnemyFall()

    {

        defeatedEnemies++;
        if (defeatedEnemies == totalEnemies)
            OnAllEnemiesKilled();

    }
    public void Reset()
    {
        defeatedEnemies = 0;
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
        else
            Start();
    }
}
       