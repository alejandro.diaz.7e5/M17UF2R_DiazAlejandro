using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitArea : MonoBehaviour
{
    public EnemySpawner spawner;
    public RoomSpawnPoints spawnPoints;
    public enum ExitType
    {
        NextRoom,
        Shop
    }
    // Start is called before the first frame update
    public ExitType exitType;
    new SpriteRenderer renderer;
    Color color;
    Coroutine anim;
    public bool isActive;
    
    void Start()
    {
       
        renderer = GetComponent<SpriteRenderer>();
        Deactivate();
        if (spawner != null)
        {
            spawner.OnStart += Deactivate;
            spawner.OnAllEnemiesKilled += Activate;
        }
        else
            Activate();
    }
    IEnumerator Animate()
    {
        bool animSwitch = false;
        while (isActive) {
            renderer.color = color;
            color.a += animSwitch ? 0.1f : -0.1f;
            animSwitch = color.a < 0 ? true : color.a > 1? false : animSwitch;
            yield return new WaitForSeconds(0.05f);
           
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        NextRoom(collision.gameObject);
    }

    public void NextRoom (GameObject player)
    {
        MusicManager mm = GameObject.FindObjectOfType<MusicManager>();
        player.transform.Find("Main Camera").GetComponent<AudioSource>().Stop();
        switch (exitType)
        {
            case ExitType.NextRoom:
                player.transform.position = spawnPoints.spawnPoints[spawnPoints.proceduralSpawnSequence[spawnPoints.CurrentRoom]];
                mm.PlayMusic(0);
                spawnPoints.Next();
                player.GetComponent<PlayerController>().data.completedRooms++;
                player.GetComponent<PlayerController>().data.score += 200;
                break;
            case ExitType.Shop:
                mm.PlayMusic(1);
                player.transform.position = spawnPoints.shopSpawnPoint;
                break;
        }
    }

    public void Activate()
    {
        GetComponent<Collider2D>().enabled = true;
        isActive = true;
        anim = StartCoroutine(Animate());
    }
    public void Deactivate()
    {
        GetComponent<Collider2D>().enabled = false;
        isActive = false;
        color = renderer.color;
        color.a = 0;
        renderer.color = color;
    }
}
