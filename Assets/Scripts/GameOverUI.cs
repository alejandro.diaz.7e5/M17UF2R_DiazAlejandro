using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GameOverUI : MonoBehaviour
{
    public TMP_Text ScoreTxt;
    public TMP_Text HighScoreTxt;
    public TMP_Text CompletedRooms;
    public TMP_Text DefeatedEnemies;
    public TMP_Text[] LastScoreTxt;
    public Image Highscore;
    public PlayerData player;
    public SavedData save;
    // Start is called before the first frame update
    void Start()
    {
        ScoreTxt.text = "Score: " + player.score;
        save.LoadFile();
        if (save.bestScore < player.score)
        {
            save.bestScore = player.score;
            Highscore.enabled = true;
        }

        ScoreTxt.text = "Score: "+ player.score;
        HighScoreTxt.text ="Highscore: "+ save.bestScore;
        CompletedRooms.text = "Completed Rooms: " + player.completedRooms;
        DefeatedEnemies.text = "Defeated Enemies: " + player.defeatedEnemies;
        Queue<int> scorehistory = new Queue<int>(save.lastScores);
        scorehistory.Dequeue();
        scorehistory.Enqueue(player.score);
        save.lastScores = scorehistory.ToArray();
        for(int i = 0;  i < LastScoreTxt.Length ;i++)
            LastScoreTxt[i].text = save.lastScores[i].ToString();
        save.SaveFile();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
