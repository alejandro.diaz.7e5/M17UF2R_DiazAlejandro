using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ItemBase;

public class InventoryManager : MonoBehaviour
{
    public PlayerData data;
    protected WeaponData[] PlayerWeapons;
    [SerializeField]
    protected List<ItemBase> Inventory;
    public Image[] weaponSlots;
    public Image[] inventorySlots;
    public int selectedItem;


    // Start is called before the first frame update
    protected virtual void Start()
    {
        LoadData();
        ShowData();
    }

    private void LoadData()
    {
        PlayerWeapons = data.weapons;
        Inventory = data.items;
    }
    protected void ShowData()
    {
        if (PlayerWeapons != null)
            for (int i = 0; i < 5; i++)
            { 
                if (PlayerWeapons[i] == null) break;
                weaponSlots[i].sprite = PlayerWeapons[i].Image;
                weaponSlots[i].color = PlayerWeapons[i].color;
            }
        for (int i = 0; i < Inventory.Count; i++)
        {
            Debug.Log("Loading "+ i);
            inventorySlots[i].sprite = Inventory[i].Image;
        }
        if (inventorySlots.Length > Inventory.Count)
        inventorySlots[Inventory.Count].sprite = null;
    }
    public void SetSelectedItem(string slotRaw)
    {
        string[]slotData = slotRaw.Split(' ');
        selectedItem = Convert.ToInt32(slotData[1])-1;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Use(){
        switch (Inventory[selectedItem].type){
            case itemType.weapon:
                GameObject.Find("Player").GetComponent<PlayerController>().weaponSelected = selectedItem;
                GameObject.Find("Player").GetComponent<PlayerController>().weaponC.changeWeapon();
                break;
            case itemType.item:
                Inventory[selectedItem].Use();
                ShowData();
                break;
        }
    }
}
