using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public AudioSource source;
    public MusicList musicList;

    public void PlayMusic(int index)
    {
        source.PlayOneShot(musicList.Music[index]);
    }

}
