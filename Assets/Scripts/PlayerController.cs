using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    MouseTracker tracker;
    public PlayerData data;
    Rigidbody2D rb;
    public GameObject healthBar;
    public GameObject weapon;
    public WeaponController weaponC; 
    public int weaponSelected = 0;
    float InputH;
    float InputV;
    float InputScroll;
    TMPro.TMP_Text Score;
    TMPro.TMP_Text Coins;
    public float speed = 3;
    public float defenseMultiplier;
    public float speedMultiplier;
    public GameObject inventory;
    private bool IsDashing;
    public AudioClip death;
    public SceneLoader loader;

    // Start is called before the first frame update
    void Start()
    {
        data.health = 10;
        data.score = 0;
        data.defeatedEnemies = 0;
        data.completedRooms = 0;
        tracker = GameObject.Find("Main Camera").GetComponent<MouseTracker>();
        rb = GetComponent<Rigidbody2D>();
        weaponC = weapon.GetComponent<WeaponController>();
        weaponC.data = data.weapons[weaponSelected];
        Score = GameObject.Find("Score").GetComponent<TMPro.TMP_Text>();
        Coins = GameObject.Find("Coins").GetComponent<TMPro.TMP_Text>();

        defenseMultiplier = 1;
        speedMultiplier = 1;
        
    }

    // Update is called once per frame
    void Update()
    {

        InputH = Input.GetAxis("Horizontal");
        InputV = Input.GetAxis("Vertical");
        InputScroll = Input.GetAxis("Mouse ScrollWheel");

        healthBar.transform.localScale = new Vector3(data.health, 1, 1);
        Score.text = $"Score: {data.score}";
        Coins.text = $"Coins: {data.coins}";

        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(Dash());
            IsDashing = true;
        }
        GetComponent<SpriteRenderer>().flipX = tracker.mousePos.x < transform.position.x;
        GetComponent<Animator>().SetBool("IsWalking", InputH != 0 || InputV != 0);
        if (!IsDashing)
            rb.velocity = new Vector3(InputH, InputV).normalized * speed * speedMultiplier;
        if (InputScroll > 0)
            NextWeapon();
        else if (InputScroll < 0)
            PrevWeapon();
        if (Input.GetKeyDown(KeyCode.E))
            ToggleInventory();



    }
    public void Damaged(float damage) {
        data.health -= damage/defenseMultiplier;
        GetComponent<Animator>().SetTrigger("Hit");
        if (data.health <= 0)
              StartCoroutine(Die());
    }

    private IEnumerator Die()
    {

        GetComponent<AudioSource>().PlayOneShot(death);
        yield return new WaitForSeconds(1);
        loader.LoadScene("DefeatScene");

        
    }

    void NextWeapon()
    {
        weaponSelected++;
        if (data.weapons[weaponSelected] == null)
            weaponSelected = 0;
        weaponC.data = data.weapons[weaponSelected];
        weaponC.changeWeapon();
    }
    void PrevWeapon()
    {
        weaponSelected--;
        if (weaponSelected == -1)
        {
            weaponSelected = 4;
            while (data.weapons[weaponSelected] == null)
                weaponSelected--;
        }
        weaponC.data = data.weapons[weaponSelected];
        weaponC.changeWeapon();

    }
    IEnumerator Dash()
    {

        Debug.Log("Dashing");
        var mousep = tracker.mousePos;
        mousep.z = 0;
        rb.velocity = (mousep - transform.position).normalized * 30;
        tag = "Invulnerable";
        GetComponent<SpriteRenderer>().color = Color.yellow;
        yield return new WaitForSeconds(0.15f);
        Debug.Log("Dash End");
        IsDashing = false;
        tag = "Player";
        GetComponent<SpriteRenderer>().color = Color.white;
        yield return null;
    }
    void ToggleInventory()
    {
        inventory.SetActive(!inventory.activeSelf); 
    }
}
