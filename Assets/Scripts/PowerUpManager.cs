using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpManager : MonoBehaviour
{
    public void DamageMultiplyBegin(float multiplier, float duration)
    {
    StartCoroutine(DamageMultiply(multiplier, duration));
    }
    IEnumerator DamageMultiply(float multiplier, float duration)
    {
        Debug.Log("beep");
        WeaponController WC = GetComponent<PlayerController>().weapon.GetComponent<WeaponController>();
        WC.DamageMultiplier = multiplier;
        yield return new WaitForSeconds(duration);
        Debug.Log("boop");
        WC.DamageMultiplier = 1;
    }
    public void DefenseMultiplyBegin(float multiplier, float duration)
    {
    StartCoroutine(DefenseMultiply(multiplier, duration));
    }
    IEnumerator DefenseMultiply(float multiplier, float duration)
    {
        Debug.Log("beep");
        PlayerController pc = GetComponent<PlayerController>();
        pc.defenseMultiplier = multiplier;
        yield return new WaitForSeconds(duration);
        Debug.Log("boop");
        pc.defenseMultiplier = 1;
    }
    public void SpeedMultiplyBegin(float multiplier, float duration)
    {
    StartCoroutine(SpeedMultiply(multiplier, duration));
    }
    IEnumerator SpeedMultiply(float multiplier, float duration)
    {
        Debug.Log("beep");
        PlayerController pc = GetComponent<PlayerController>();
        pc.speedMultiplier = multiplier;
        yield return new WaitForSeconds(duration);
        Debug.Log("boop");
        pc.speedMultiplier = 1;
    }
}
