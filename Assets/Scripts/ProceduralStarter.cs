using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralStarter : MonoBehaviour
{
    public RoomSpawnPoints spawnPoints;
    // Start is called before the first frame update
    void Start()
    {
        spawnPoints.GenerateSequence();
    }

}
