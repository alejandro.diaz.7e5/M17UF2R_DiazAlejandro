using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public GameObject message;
    public GameObject shopUI;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        message.SetActive(true);
        this.enabled = true;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            shopUI.SetActive(!shopUI.activeSelf);       
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        message.SetActive(false);
        this.enabled = false;
        if (shopUI.activeSelf) shopUI.SetActive(false);
    }

}
