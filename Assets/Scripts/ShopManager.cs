using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : InventoryManager
{
    public List<Button> buyButtons;
    protected override void Start()
    {
        ShowData();
        ShowPrices();
    }

    private void ShowPrices()
    {
        for (int i = 0; i < buyButtons.Count; i++)
        {
            buyButtons[i].GetComponentInChildren<TMPro.TMP_Text>().text = Inventory[i].BasePrice.ToString();
            if (data.coins >= Inventory[i].BasePrice) 
            {
                buyButtons[i].image.color = Color.green;
                buyButtons[i].interactable = true;
            }
            else
            {
                buyButtons[i].image.color = Color.red;
                buyButtons[i].interactable = false;
            }
        }
    }

    public void Buy(int button)
    {
        data.coins -=(int) Inventory[button].BasePrice;
        switch (Inventory[button].type)
        {
            case ItemBase.itemType.weapon:
                for (int i = 0; i < data.weapons.Length; i++)
                {
                    if (data.weapons[i] == null)
                    {
                        data.weapons[i] = (WeaponData) Inventory[button];
                        break;
                    }
                    else if (data.weapons[i] == (WeaponData)Inventory[button])
                    {
                        data.weapons[i].ammo += data.weapons[i].loadValue;
                        break;
                    }
                }
                break;
            case ItemBase.itemType.item:
                    data.items.Add(Inventory[button]);
                    break;
        }
        ShowPrices();
    }
}

