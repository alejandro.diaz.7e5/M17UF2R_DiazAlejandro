using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{
    public List<EnemySpawner> spawners;
    // Start is called before the first frame update
    public void ActivateSpawn(int spawnIndex) 
    { 
        spawners[spawnIndex].Reset(); 
    }
}
