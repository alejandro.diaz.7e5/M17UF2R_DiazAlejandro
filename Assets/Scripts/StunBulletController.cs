using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunBulletController : BulletControllerStraight
{
    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (collision.gameObject.tag == "Enemy")
            collision.gameObject.GetComponent<EnemyController>().Stunned();
    }
}
