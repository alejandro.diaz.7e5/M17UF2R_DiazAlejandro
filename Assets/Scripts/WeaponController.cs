using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    protected int ammoType;
    public WeaponData data;
    public float DamageMultiplier;
    MouseTracker tracker;
    Vector3 direction;
    public GameObject bullet;
    float click;
    float shotCoolDown;
    TMPro.TMP_Text ammoDisplay;
    // Start is called before the first frame update

    void Start()
    {
        DamageMultiplier = 1;
        tracker = GameObject.Find("Main Camera").GetComponent<MouseTracker>();
        ammoDisplay = GameObject.Find("Ammo").GetComponent<TMPro.TMP_Text>();
        changeWeapon();

    }

    public void changeWeapon()
    {
        bullet = data.bullet;
        GetComponent<SpriteRenderer>().sprite = data.sprite;
        GetComponent<SpriteRenderer>().color = data.color;
    }

    // Update is called once per frame
    void Update()
    {
        ammoDisplay.text = $"Ammo: {data.ammo}";
        click = Input.GetAxis("Fire1");
        if (click == 1 && shotCoolDown <= 0 && data.ammo > 0)
        {
            shoot();
            shotCoolDown = data.shotCoolDown;
        }
        if (shotCoolDown >= 0)
            shotCoolDown -= Time.deltaTime;
        direction = tracker.mousePos - transform.position;
        transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg);
        GetComponent<SpriteRenderer>().flipY = tracker.mousePos.x < transform.position.x;
    }

    void shoot()
    {
        switch (data.WeaponShot)
            {
            case WeaponData.ShotType.Single:
                ShotSingle();
                break;
            case WeaponData.ShotType.Dispersion:
                ShotDispersion();
                break;
            default:
                break;
        }
    }

    private void ShotDispersion()
    {
        Debug.Log("Shooting");
        for (int i = 20; i > -20; i-=10)
        {
            GameObject shot = GameObject.Instantiate(bullet, new Vector3(transform.position.x, transform.position.y), transform.rotation * Quaternion.Euler(0,0,i));
            shot.GetComponent<Damager>().damage = data.damage * DamageMultiplier;

        }
        data.ammo--;
    }

    void ShotSingle()
    {
        Debug.Log("Shooting");
        GameObject shot = GameObject.Instantiate(bullet, new Vector3(transform.position.x, transform.position.y), transform.rotation);
        shot.GetComponent<Damager>().damage = data.damage * DamageMultiplier;
        data.ammo--;
    }
}