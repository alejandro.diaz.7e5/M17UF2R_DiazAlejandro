# The Fire Room
**The fire room** is a game half roguelike, half shooter 2D which wants to provide the player classic arcade fun without complex stories or narrative. Just a simple game to have fun killing enemies while make your way towards the exit.

# Technical details
**Genre**: Roguelike, 2D Shooter
**Platform**: PC
 **Players**: 1 Player
 **Engine**: Unity 3D
 **Graphics**: Cartoon.

# Controls

WASD = Movement
Left Click = Shoot
MouseWheel = Change Weapon
Q to Access Shop
E to Open Inventory
Space to Dash

# Diagrama

https://drive.google.com/file/d/1AMmNhOgNOZ5USfDzSd-6l3TrfUjeGhJJ/view?usp=sharing

# Changes/Upgrades
-Inventory System Reworked
-Spawners Updated
-PowerUps
-Currency added
-Shop added
-Rooms added
-Saves Added (Score)
-Stun gun
-Music 
-Procedural Progression

